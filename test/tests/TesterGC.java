
package tests;

import data.Dao;
import entites.Client;
import org.junit.Test;
import entites.Region;
import static org.junit.Assert.*;
import org.junit.Before;

public class TesterGC {
      
    Dao dao;
    
    @Before
    public  void initialisationDAO() {
    
        dao=new Dao();
    }
    
    @Test
    public void testerCaclientParAnnee() {

         Client c=dao.getClientDeNumero(101L);
         
         assertEquals("Erreur Calcul CA",575.5, c.caClient(2018),0.001);
    }
    
    @Test
    public void testerCaregionParAnnee(){
        
       Region r=dao.getRegionDeCode("HDF");
       //Addition des montants des factures des clients de la region HDF pour l'année 2018
       assertEquals("Erreur Calcul CA",1510.64,r.caRegion(2018),0.001);
       
        
    }
    
    @Test public void testerCaregionParAnneeEtMois(){
        
        Region r=dao.getRegionDeCode("HDF");
       //Addition des montants des factures des clients de la region HDF pour l'année 2018 et le mois de janvier
       assertEquals("Erreur Calcul CA",1132.3,r.caRegion(2018,01),0.001);
        
    }
    //   //<editor-fold defaultstate="collapsed" desc="TEST CA PAR ANNEE ET MOIS A FAIRE">
    
        @Test
        public void testerCaclientParAnneeEtMois() {
    
             Client c=dao.getClientDeNumero(101L);
    
             assertEquals("Erreur Calcul CA",1131.28, c.caClient(2017,12),0.001);
        }
        
    //</editor-fold>
        
        
}